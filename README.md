This repo contains code for running a hello world program by submitting it to slurm workload manager on Spartan hpc using Cromwell / wdl workflow system. The code borrows from the following sources:

https://gitlab.unimelb.edu.au/bshaban/metaGenePipe
https://cromwell.readthedocs.io/en/stable/tutorials/FiveMinuteIntro/

To execute this code..

ssh into Spartan. per https://dashboard.hpc.unimelb.edu.au/getting_started/

run the following commands

git clone https://gitlab.unimelb.edu.au/macunningham/spartan_workflow.git

cd spartan_workflow/

wget https://github.com/broadinstitute/cromwell/releases/download/45.1/cromwell-45.1.jar

module load Java

java -Dconfig.file=./cromslurm.conf -jar cromwell-45.1.jar run myworkflow.wdl